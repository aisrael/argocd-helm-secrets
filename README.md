argocd-helm-secrets
====

Extends the `argoproj/argocd` Docker image with [SOPS](https://github.com/mozilla/sops) and [helm-secrets](https://github.com/zendesk/helm-secrets), following [this ITNEXT article on Medium.com](https://itnext.io/argocd-a-helm-chart-deployment-and-working-with-helm-secrets-via-aws-kms-96509bfc5eb3).
