.PHONY: docker

ARGOCD_VERSION ?= "v2.2.3"
SOPS_VERSION ?= "v3.7.1"
HELM_SECRETS_VERSION ?= "2.0.2"

CI_REGISTRY_IMAGE ?= registry.gitlab.com/aisrael/argocd-helm-secrets

CI_COMMIT_SHA = $(shell git rev-parse HEAD)

docker:
	[ -z "$$CI_REGISTRY_IMAGE" ] && CI_REGISTRY_IMAGE=$(CI_REGISTRY_IMAGE); \
	[ -z "$$CI_COMMIT_SHA" ] && CI_COMMIT_SHA=$(CI_COMMIT_SHA); \
	[ -z "$$CI_COMMIT_SHORT_SHA" ] && CI_COMMIT_SHORT_SHA=$${CI_COMMIT_SHA:0:8}; \
	docker build --build-arg SHIPPY_ACCESS_KEY=$(SHIPPY_ACCESS_KEY) \
	--build-arg ARGOCD_VERSION=$(ARGOCD_VERSION) \
	--build-arg SOPS_VERSION=$(SOPS_VERSION) \
	--build-arg PYTHON_VERSION=$(PYTHON_VERSION) \
	-t $${CI_REGISTRY_IMAGE}:$${CI_COMMIT_SHORT_SHA} \
	-t $${CI_REGISTRY_IMAGE}:$(ARGOCD_VERSION) \
	-t $${CI_REGISTRY_IMAGE}:latest .
